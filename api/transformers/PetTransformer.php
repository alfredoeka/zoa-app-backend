<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/21/2017
 * Time: 9:00 PM
 */

namespace api\transformers;

use League\Fractal\TransformerAbstract;
use api\models\Pet;

class PetTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'detail',
        'owner'
    ];

    public function transform(Pet $pet)
    {
        return [
            'id'        => (int) $pet->id,
            'name'      => $pet->name,
            'gender'    => $pet->gender
        ];
    }

    public function includeDetail(Pet $pet)
    {
        $detail = $pet->detail;
        return $this->item($detail, new PetDetailTransformer());
    }

    public function includeOwner(Pet $pet)
    {
        $detail = $pet->user;
        return $this->item($detail, new UserProfileTransformer());
    }
}