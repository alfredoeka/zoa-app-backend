<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/21/2017
 * Time: 10:36 PM
 */

namespace api\transformers;


use api\models\PetRace;
use League\Fractal\TransformerAbstract;

class PetRaceTransformer extends TransformerAbstract
{
    public function transform(PetRace $petRace)
    {
        return [
            'id'            => $petRace->id,
            'pet_family_id' => $petRace->pet_family_id,
            'name'          => $petRace->name
        ];
    }
}