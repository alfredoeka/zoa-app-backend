<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/21/2017
 * Time: 10:29 PM
 */

namespace api\transformers;


use api\models\PetClass;
use League\Fractal\TransformerAbstract;

class PetClassTransformer extends TransformerAbstract
{
    public function transform(PetClass $class)
    {
        return [
            'id'    => (int) $class->id,
            'name'  => $class->name
        ];
    }
}