<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/21/2017
 * Time: 10:29 PM
 */

namespace api\transformers;


use api\models\PetDetail;
use League\Fractal\TransformerAbstract;

class PetDetailTransformer extends TransformerAbstract
{
    public function transform(PetDetail $detail)
    {
        return [
            'id'            => (int) $detail->id,
            'pet_family_id' => $detail->pet_family_id,
            'pet_race_id'   => $detail->pet_race_id
        ];
    }
}