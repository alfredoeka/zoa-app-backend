<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/22/2017
 * Time: 10:13 AM
 */

namespace api\transformers;


use api\models\PetRoutine;
use League\Fractal\TransformerAbstract;

class PetRoutineTransformer extends TransformerAbstract
{
    public function transform(PetRoutine $routine)
    {
        return [
            'id' => $routine->id
        ];
    }
}