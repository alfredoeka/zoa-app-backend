<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/20/2017
 * Time: 9:04 AM
 */

namespace api\controllers;

use api\controllers\BaseController;
use api\models\Pet;
use api\models\PetClass;
use api\models\PetFamily;
use api\models\PetRace;
use api\transformers\PetClassTransformer;
use api\transformers\PetFamilyTransformer;
use api\transformers\PetRaceTransformer;
use api\transformers\PetTransformer;
use Yii;
use yii\base\Module;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use api\controllers\UserController;

class PetController extends BaseController {
    public $enableCsrfValidation = false;

    private $user;

    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        /*
         * Set variable user with value of current authenticated user
         * */
        $this->user = Yii::$app->user->identity;
    }

    public function behaviors()
    {
        return [
             'authMethods' => [
                 'class' => CompositeAuth::className(),
                 'authMethods' => [
                     HttpBearerAuth::className(),
                 ]
             ],
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
            'verbs' => [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'index'  => ['GET'],
                    'view'   => ['GET'],
                    'create' => ['GET', 'POST'],
                    'update' => ['GET', 'PUT', 'POST'],
                    'delete' => ['POST', 'DELETE'],
                    'register' => ['POST']
                ],
            ],
        ];
    }


    /**
     * Register a pet info
     * @return array|mixed
     */
    public function actionRegister()
    {
        return Yii::$app->request->bodyParams;
    }

    /**
     * Fetch user's pets data
     * @return array
     */
    public function actionList()
    {
        $pet = $this->user->pet;
        return $this->collection($pet, new PetTransformer(), 'pet_list');
    }

    /**
     * Get animal classes information
     * @param null $id
     * @return array
     */
    public function actionClass($id = null)
    {
        if ($id) {
            $petClass = PetClass::findOne($id);
            return $this->item($petClass, new PetClassTransformer(), 'pet_class');
        }

        $petClass = PetClass::find()->all();
        return $this->collection($petClass, new PetClassTransformer(), 'pet_class_index');
    }

    /**
     * Get animal families information
     * @param null $id
     * @param null $classId
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionFamily($id = null, $classId = null)
    {
        if ($classId)
            return $this->collection(PetFamily::find()->where(['pet_class_id' => $classId])->all(), new PetFamilyTransformer(),'pet_families_by_class_id');
        if ($id)
            return $this->item(PetFamily::findOne($id), new PetFamilyTransformer(), 'pet_family');
        return PetFamily::find()->all();
    }

    /**
     * Get animal races information
     * @param null $id
     */
    public function actionRaces($id = null, $familyId = null)
    {
        if ($id)
            return $this->item(PetRace::findOne($id), new PetRaceTransformer(), 'pet_races');
        if ($familyId)
            return $this->collection(PetRace::find()->where('pet_family_id', $familyId), new PetRaceTransformer(), 'pet_races_by_family_id');
        return $this->collection(PetRace::find()->all(), new PetRaceTransformer(), 'pet_races');
    }

    public function actionProfile($id)
    {
        return $this->item(Pet::findOne($id), new PetTransformer(), 'pet_profile');
    }

}