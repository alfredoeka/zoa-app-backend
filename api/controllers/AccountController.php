<?php
namespace api\controllers;

use api\models\AccountVerificationToken;
use Carbon\Carbon;
use Yii;
use api\models\User;
use api\controllers\BaseController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use yii\base\InvalidRouteException;
use api\transformers\UserTransformer;
use yii\base\DynamicModel;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;

class AccountController extends BaseController {

    public $modelClass = 'api\models\User';
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'login'  => ['post'],
                    'register'  => ['post'],
                    'verify' => ['get']
                ],
            ],
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::className(),
                'only' => ['index', 'view', 'verify-password-reset-token'],
                'formatParam' => '_format',
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                    'application/xml' => \yii\web\Response::FORMAT_XML,
                ],
            ],
        ];
    }

    public function actionTest()
    {
        $request = Yii::$app->getRequest()->getBodyParams();
        $id_token = $request['id_token'];
        
        $client = new \Google_Client(['client_id' => "228866141337-vgbd8ainidihr3ogvv4pf9erqd2st7s3.apps.googleusercontent.com"]);

        $payload = $client->verifyIdToken($id_token);

        if ($payload) {
            $userid = $payload['sub'];
            // If request specified a G Suite domain:
            //$domain = $payload['hd'];
            return ["user_id" => $userid];
        } else {
            // Invalid ID token
            return ["status" => "gagal", "request" => $request];
        }
    }

    public function actionRegister($vendor = null)
    {
        $request = Yii::$app->request->bodyParams;
        switch ($vendor) {
            case "google" :
                $model = new User(['scenario' => User::SCENARIO_VERIFY_GOOGLE_ID_TOKEN]);
                $model->attributes = $request;
                if (!$model->validate()) {
                    return $model->errors;
                }
                //$model->save();
                $id_token = $request['id_token'];
                $client = new \Google_Client(['client_id' => "493043003889-18jjdddn8jlh07fvgcpb6keblti5tpn3.apps.googleusercontent.com"]);
                $payload = $client->verifyIdToken($id_token);

                if ($payload) {
                    $userid = $payload['sub'];

                    $account = User::find()
                                    ->where(['email' => $payload['email']])
                                    ->one();

                    if (count($account) != 0) {
                        return [
                            "success" => true,
                            "data" => [
                                "zoa_user_id" => $account->getId(),
                                "zoa_access_token" => $account->getAttribute("access_token"),
                                "google_credential" => $payload,
                                "status_code" => 200,
                                "message" => "ID Token Verified",
                            ]
                        ];
                    }

                    $model = new User(['scenario' => User::SCENARIO_REGISTER_WITH_GOOGLE]);
                    $model->first_name = $payload['given_name'];
                    $model->last_name = $payload['family_name'];
                    $model->email = $payload['email'];
                    $model->picture = $payload['picture'];
                    $model->access_token = md5($rand = substr(md5(microtime()),rand(0,26),5));
                    $model->is_verified = true;

                    if (!$model->save(true)) {
                        return $model->errors;
                    }
                    // If request specified a G Suite domain:
                    //$domain = $payload['hd'];
                    return [
                        "success" => true,
                        "data" => [
                            "zoa_user_id" => $model->getId(),
                            "zoa_access_token" => $model->getAttribute("access_token"),
                            "google_credential" => $payload,
                            "status_code" => 200,
                            "message" => "ID Token Verified",
                        ]
                    ];
                } else {
                    return [
                        "success" => false,
                        "error" => [
                            "code" => 403,
                            "message" => "Invalid token"
                        ]
                    ];
                }
                break;
            default:
                $model = new User();
                $model->scenario = $model::SCENARIO_REGISTER;
                $model->first_name = $request['first_name'];
                $model->last_name = $request['last_name'];
                $model->username = $request['username'];
                $model->email = $request['email'];
                $model->password = Yii::$app->security->generatePasswordHash($request['password']);
                $model->access_token = md5($rand = substr(md5(microtime()),rand(0,26),5));
                $model->device_token = $request['device_token'];
                $model->is_verified = false;

                if (!$model->save(true)) {
                    return $model->errors;
                }

                $model = User::findOne($model->getId());
                $model->setVerificationToken(1);

                $response = [
                    'status' => 200,
                    'message' => 'User registered',
                    'data' => [
                        "First Name" => $model->username,
                        "Email" => $model->email,
                        "accessToken" => $model->access_token
                    ]
                ];

                return $response;
                break;
        }
    }

    public function actionLogin($vendor = null)
    {
        $request = Yii::$app->request->bodyParams;

        if ($vendor == null)  {
            $model = DynamicModel::validateData($request, [
                [['email', 'password'], 'required', 'message' => 'Field required']
            ]);

            if (!$model->validate()) {
                return $model->errors;
            }

            $email = $request['email'];
            $password = $request['password'];

            $user = User::findOne([
                'email' => $email,
                'password' => Yii::$app->security->generatePasswordHash($password)
            ]);

            if (!$user) {
                throw new UnauthorizedHttpException($mssage = "Unauthorized Account");
            }

            return $this->item($user, new UserTransformer(), 'user');
        }


        switch ($vendor) {
            case 'google':
                $id_token = $request['id_token'];
                $client = new \Google_Client(['client_id' => "956119802934-toedfvs3s1m2d423km1unq69rq6dp3m9.apps.googleusercontent.com"]);
                $payload = $client->verifyIdToken($id_token);

                $account = User::find()
                    ->where(['email' => $payload['email']])
                    ->one();

                if (count($account) != 0) {
                    return [
                        "success" => true,
                        "data" => [
                            "zoa_user_id" => $account->getId(),
                            "zoa_access_token" => $account->getAttribute("access_token"),
                            "google_credential" => $payload,
                            "status_code" => 200,
                            "message" => "ID Token Verified",
                        ]
                    ];
                }

                $model = new User(['scenario' => User::SCENARIO_REGISTER_WITH_GOOGLE]);
                $model->first_name = $payload['given_name'];
                $model->last_name = $payload['family_name'];
                $model->email = $payload['email'];
                $model->picture = $payload['picture'];
                $model->access_token = md5($rand = substr(md5(microtime()),rand(0,26),5));

                if (!$model->save(true)) {
                    return $model->errors;
                }
                // If request specified a G Suite domain:
                //$domain = $payload['hd'];
                return [
                    "success" => true,
                    "data" => [
                        "zoa_user_id" => $model->getId(),
                        "zoa_access_token" => $model->getAttribute("access_token"),
                        "google_credential" => $payload,
                        "status_code" => 200,
                        "message" => "ID Token Verified",
                    ]
                ];

                break;
            case 'facebook':
                return "login karo facebook";
                break;
            default :
                throw new InvalidRouteException();
                break;
        }
    }

    public function actionVerify($token, $type)
    {
        $token = AccountVerificationToken::find()->where(['token' => $token, 'type' => $type])->one();
        $isTokenExpired = ($token->expired_at > Carbon::now("Asia/Jakarta")->toDateTimeString()) ? false : true ;
        if (!$isTokenExpired) {
            $user = $token->user;
            $user->is_verified = true;
            $user->save();
            return "Verified";
        } else {
            return "Verification Failed";
        }
    }

    public function actionResetPasswordUserSearch()
    {
        $param = Yii::$app->request;
        $user = User::find()->where([
            'or',
            ['email' => $param->get("email")],
            ['username' => $param->get("username")]
        ])->one();

        if ($user) {
            return [
                "success" => true,
                "data" => [
                    "user" => [
                        "id" => $user->id,
                        "username" => $user->username,
                        "picture" => $user->picture,
                        "email" => $user->email
                    ],
                    "message" => "User found"
                ]
            ];
        }

        return [
            "success" => false,
            "data" => [
                "status_code" => 404,
                "message" => "Username not found"
            ]
        ];
    }

    public function actionSendEmailResetPassword($email)
    {

        /* @var $user \common\models\User */
        $user = \common\models\User::findOne([
            'email' => $email,
        ]);

        if (!$user) {
            return false;
        }

        if (!\common\models\User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        $send = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom("zoa@outlook.co.id")
            ->setTo($email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();

        if ($send)
        {
            return [
                "success" => true,
                "data" => [
                    "status_code" => 200,
                    "message" => "Email for password reset sent"
                ]
            ];
        }

        return [
            "success" => false,
            "data" => [
                "status_code" => 400,
                "message" => "Email not failed to send"
            ]
        ];
    }
}