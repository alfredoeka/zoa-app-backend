<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/22/2017
 * Time: 10:05 AM
 */

namespace api\models;


use yii\db\ActiveRecord;

class PetRoutine extends ActiveRecord
{
    public static function tableName()
    {
        return 'pet_routines';
    }
}