<?php

namespace api\models;

use Carbon\Carbon;
use phpseclib\Crypt\Random;
use Yii;
use yii\db\ActiveRecord;
use yii\base\Model;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public $id_token;

    const SCENARIO_LOGIN = 'login';
    const SCENARIO_REGISTER = 'register';
    const SCENARIO_VERIFY_GOOGLE_ID_TOKEN = 'verify_google_id_token';
    const SCENARIO_REGISTER_WITH_GOOGLE = 'register_google';
    const SCENARIO_DEFAULT = 'default';

    public function scenarios()
    {
        return [
            self::SCENARIO_LOGIN => ['username', 'password'],
            self::SCENARIO_REGISTER => ['username', 'email', 'password'],
            self::SCENARIO_VERIFY_GOOGLE_ID_TOKEN => ['id_token'],
            self::SCENARIO_REGISTER_WITH_GOOGLE => ['first_name', 'email'],
            self::SCENARIO_DEFAULT => ['username', 'email', 'password']
        ];
    }

    public function rules() {
        return [
            [['username', 'email', 'password'], 'required', 'on' => self::SCENARIO_REGISTER],
            ['id_token', 'required', 'on' => self::SCENARIO_VERIFY_GOOGLE_ID_TOKEN],
            [['email'], 'email'],
            [['email'], 'unique']

        ];
    }

    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        // return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // foreach (self::$users as $user) {
        //     if ($user['accessToken'] === $token) {
        //         return new static($user);
        //     }
        // }

        // return null;

        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        // foreach (self::$users as $user) {
        //     if (strcasecmp($user['username'], $username) === 0) {
        //         return new static($user);
        //     }
        // }

        // return null;

        return static::findOne(['username' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function getPet()
    {
        return $this->hasMany(Pet::className(), ['user_id' => 'id']);
    }

    public function setVerificationToken($type)
    {
        $now = Carbon::now("Asia/Jakarta");
        $tokenExpirationTime = $now->copy()->addHour(2)->toDateTimeString();
        $token = $this->getVerificationToken()->where(['type' => $type])->one();
        if (count($token)) {
            $token->token = self::genRandStr();
            $token->expired_at = $tokenExpirationTime;
            $token->type = 1;
            $token->updated_at = $now;
            $token->save();
            AccountVerificationToken::sendVerification($token->token, $token->type, $this->email);
        } else {
            $token = new AccountVerificationToken();
            $token->type = 1; // Email Verification
            $token->token = self::genRandStr();
            $token->expired_at = $tokenExpirationTime;
            $token->created_at = $now;
            $token->link('user', $this);
            AccountVerificationToken::sendVerification($token->token, $token->type, $this->email);
        }
    }

    static function genRandStr(){
        $a = $b = '';

        for($i = 0; $i < 3; $i++){
            $a .= chr(mt_rand(65, 90)); // see the ascii table why 65 to 90.
            $b .= mt_rand(0, 9);
        }

        return $a . $b;
    }

    public function getVerificationToken()
    {
        return $this->hasOne(AccountVerificationToken::className(), ['user_id' => 'id']);
    }
}
