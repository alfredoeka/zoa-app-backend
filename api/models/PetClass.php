<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/20/2017
 * Time: 10:30 AM
 */

namespace api\models;


use yii\db\ActiveRecord;

class PetClass extends ActiveRecord
{
    public static function tableName()
    {
        return 'pet_classes';
    }

    public function getFamilies()
    {
        return $this->hasOne(PetFamily::className(), ['pet_class_id' => 'id']);
    }
}