<?php
/**
 * Created by PhpStorm.
 * User: alfredo
 * Date: 11/20/2017
 * Time: 10:30 AM
 */

namespace api\models;


use yii\db\ActiveRecord;

class PetRace extends ActiveRecord
{
    public static function tableName()
    {
        return 'pet_races';
    }
}