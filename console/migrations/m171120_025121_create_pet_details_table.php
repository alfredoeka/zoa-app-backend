<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pet_details`.
 */
class m171120_025121_create_pet_details_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pet_details', [
            'id' => $this->primaryKey(),
            'pet_id' => $this->integer(),
            'pet_family_id' => $this->integer(),
            'pet_race_id' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pet_details');
    }
}
