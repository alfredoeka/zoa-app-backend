<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pet_routine_recurring`.
 */
class m171121_172302_create_pet_routine_recurring_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pet_routine_recurring_pattern', [
            'id' => $this->primaryKey(),
            'pet_routine_id' => $this->integer(),
            'recurring_type_id' => $this->integer(),
            'separation_count' => $this->integer(),
            'max_num_occurences' => $this->integer(),
            'day_of_week' => $this->integer(),
            'week_of_month' => $this->integer(),
            'day_of_month' => $this->integer(),
            'month_of_year' => $this->integer(),
            'created_at' => $this->timestamp(),
            'created_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pet_routine_recurring_pattern');
    }
}
