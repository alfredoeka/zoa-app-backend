<?php

use yii\db\Migration;

/**
 * Handles the creation of table `verification_token`.
 */
class m171124_084309_create_verification_token_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_verification_token', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'type' => $this->integer(),
            'token' => $this->string(6),
            'expired_at' => $this->timestamp(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_verification_token');
    }
}
