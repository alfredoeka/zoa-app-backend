<?php

use yii\db\Migration;

class m171122_010953_create_table_recurring_type extends Migration
{
    /*public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m171122_010953_create_table_recurring_type cannot be reverted.\n";

        return false;
    }*/


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('pet_routine_recurring_type', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);
    }

    public function down()
    {
        $this->dropTable('pet_routine_recurring_pattern');
    }

}
