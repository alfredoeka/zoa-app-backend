<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pet_classes`.
 */
class m171120_025212_create_pet_classes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pet_classes', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pet_classes');
    }
}
